//
//  RotaViewController.swift
//  RotaManager
//
//  Created by Sohel Seedat on 28/09/2015.
//  Copyright (c) 2015 BigSMan. All rights reserved.
//

import UIKit
import RealmSwift

class RotaViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var tableView: UITableView!
    
    let daysArray = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "DATE Rota"
        setupAppearance()
    }
    
    func setupAppearance() {
        self.tableView.separatorStyle = .None
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch (indexPath.row) {
        case 0:
            var cell = self.tableView.dequeueReusableCellWithIdentifier("HeaderCell", forIndexPath: indexPath) as! UITableViewCell
            return cell
        case 1...7:
            var cell = self.tableView.dequeueReusableCellWithIdentifier("DayCell", forIndexPath: indexPath) as! DayTableViewCell
            cell.dayLabel.text = daysArray[indexPath.row - 1]
            return cell
        default:
            var cell = self.tableView.dequeueReusableCellWithIdentifier("DayCell", forIndexPath: indexPath) as! UITableViewCell
            return cell
        }
        
    }

}
