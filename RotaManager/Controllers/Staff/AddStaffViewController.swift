//
//  AddStaffViewController.swift
//  RotaManager
//
//  Created by Sohel Seedat on 29/09/2015.
//  Copyright (c) 2015 BigSMan. All rights reserved.
//

import UIKit
import RealmSwift

class AddStaffViewController: UIViewController {

    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var saveButton: UIBarButtonItem!
    
    var staff = Staff()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func saveStaffWithName(name: String) {
        let realm = Realm()
        self.staff.name = name
        realm.write {
            realm.add(self.staff)
        }
    }
    
    
    
    @IBAction func cancelButtonPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func saveButtonPressed(sender: AnyObject) {
        saveStaffWithName(nameTextField.text)
        self.dismissViewControllerAnimated(true, completion: nil)
    }

}
