//
//  StaffViewController.swift
//  RotaManager
//
//  Created by Sohel Seedat on 29/09/2015.
//  Copyright (c) 2015 BigSMan. All rights reserved.
//

import UIKit
import RealmSwift

class StaffViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var tableView: UITableView!
    
    var staff: Results<Staff>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        staff = Realm().objects(Staff)
        self.tableView.reloadData()
    }
    
    //
    // MARK:- UITableViewDataSource
    //
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return staff.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = self.tableView.dequeueReusableCellWithIdentifier("StaffCell", forIndexPath: indexPath) as! UITableViewCell
        var staffMember = staff[indexPath.row]
        cell.textLabel?.text = staffMember.name
        cell.textLabel?.font = UIFont(name: "AvenirNext-Medium", size: 16)
        return cell
    }
    
    

}
