//
//  DayTableViewCell.swift
//  RotaManager
//
//  Created by Sohel Seedat on 28/09/2015.
//  Copyright (c) 2015 BigSMan. All rights reserved.
//

import UIKit
import RealmSwift

class DayTableViewCell: UITableViewCell, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {

    @IBOutlet var dayLabel: UILabel!
    
    @IBOutlet var view: UIView!
    @IBOutlet var openNameLabel: UITextField!
    @IBOutlet var midNameLabel: UITextField!
    @IBOutlet var closeNameLabel: UITextField!
    
    @IBOutlet var dayView: UIView!
    @IBOutlet var openView: UIView!
    @IBOutlet var midView: UIView!
    @IBOutlet var closeView: UIView!
    
    var staff: Results<Staff>!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupAppearance()
        setupNamePicker()
    }
    
    func setupAppearance() {
        dayView.layer.borderWidth = 1.0
        openView.layer.borderWidth = 1.0
        midView.layer.borderWidth = 1.0
        closeView.layer.borderWidth = 1.0
        
        staff = Realm().objects(Staff)

    }

    func setupNamePicker() {
        var pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        self.openNameLabel.inputView = pickerView
        self.midNameLabel.inputView = pickerView
        self.closeNameLabel.inputView = pickerView
    }

    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return staff.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return staff[row].name
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if openNameLabel.isFirstResponder() {
            openNameLabel.text = staff[row].name
        } else if midNameLabel.isFirstResponder() {
            midNameLabel.text = staff[row].name
        } else if closeNameLabel.isFirstResponder() {
            closeNameLabel.text = staff[row].name
        }
    }


}
